const fs = require('fs');
const WebSocket = require('ws');
const lame = require('lamejs');

const audioFile = 'Mercedes AMG C63s Drive Moderate V2.wav';
const websocketUrl = 'ws://localhost:8080'; // WebSocket server URL

const audioData = fs.readFileSync(audioFile);
const mp3Encoder = new lame.Mp3Encoder(1, 44100, 192); // Mono, sample rate 44100, bitrate 192 kbps

const websocket = new WebSocket(websocketUrl);

websocket.on('open', () => {
  console.log('WebSocket connection established!');

  const samples = new Int16Array(audioData.buffer);

  const chunkSize = 1152; // Number of audio samples per MP3 frame
  const sampleRate = 44100; // Audio sample rate
  const numChannels = 1; // Number of audio channels (mono)

  let remainingSamples = samples.length;

  for (let i = 0; i < samples.length; i += chunkSize) {
    const end = Math.min(i + chunkSize, samples.length);
    const chunk = samples.subarray(i, end);

    const mp3Buffer = mp3Encoder.encodeBuffer(chunk);
    websocket.send(mp3Buffer);

    remainingSamples -= chunk.length;
    console.log(`Streaming progress: ${Math.round((remainingSamples / samples.length) * 100)}%`);
  }

  const mp3Buffer = mp3Encoder.flush();
  websocket.send(mp3Buffer);

  websocket.close();
  console.log('Streaming complete!');
});

websocket.on('error', (error) => {
  console.error('WebSocket error:', error);
});
