const fs = require('fs');
const wav = require('wav');

// Read the WAV file
const reader = new wav.Reader();
const inputFilePath = '//home/don/Documents/walkie/Walkie-Talkie_Project/Server_NodeJS/Mercedes AMG C63s Drive Moderate V2.wav';
const outputFilePath = 'mod_mac.wav';

const inputFile = fs.createReadStream(inputFilePath);
const outputFile = fs.createWriteStream(outputFilePath);

// Extract relevant information
let bitDepth, channels, sampleRate;

reader.on('format', format => {
  bitDepth = format.bitDepth;
  channels = format.channels;
  sampleRate = format.sampleRate;
});

reader.on('data', data => {
  // Modify the buffer according to the desired changes
  const modifiedBuffer = modifyBuffer(data, bitDepth, channels, sampleRate, 32, 1, 16000);

  // Write the modified buffer to the output file
  outputFile.write(modifiedBuffer);
});

reader.on('end', () => {
  outputFile.end();
  console.log('WAV file modified successfully.');
});

inputFile.pipe(reader);

// Function to modify the buffer
function modifyBuffer(buffer, currentBitDepth, currentChannels, currentSampleRate, targetBitDepth, targetChannels, targetSampleRate) {
  // Perform your desired modifications here
  // This is a sample implementation that simply creates a new buffer with the target specifications

  const numSamples = buffer.length / (currentBitDepth / 8);
  const numChannels = currentChannels;
  const bytesPerSample = targetBitDepth / 8;
  const targetBuffer = Buffer.alloc(numSamples * numChannels * bytesPerSample);

  for (let i = 0; i < numSamples; i++) {
    const sourceOffset = i * numChannels * (currentBitDepth / 8);
    const targetOffset = i * numChannels * bytesPerSample;

    for (let j = 0; j < numChannels; j++) {
      const value = buffer.readIntLE(sourceOffset + j * (currentBitDepth / 8), currentBitDepth);
      targetBuffer.writeIntLE(value, targetOffset + j * bytesPerSample, targetBitDepth);
    }
  }

  return targetBuffer;
}
