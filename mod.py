import wave

def alter_wav(file_path, new_bitdepth=None, new_channels=None, new_sample_rate=None):
    # Open the original WAV file
    with wave.open(file_path, 'rb') as original_file:
        # Extract the original parameters
        original_params = original_file.getparams()

        # Set the new parameters
        new_channels = new_channels if new_channels else original_params.nchannels
        new_bitdepth = new_bitdepth if new_bitdepth else original_params.sampwidth * 8
        new_sample_rate = new_sample_rate if new_sample_rate else original_params.framerate

        new_params = (
            new_channels,
            new_bitdepth // 8,  # Convert bitdepth to bytes
            new_sample_rate,
            original_params.nframes,
            original_params.comptype,
            original_params.compname
        )

        # Create a new WAV file with the modified parameters
        with wave.open('altered_file.wav', 'wb') as altered_file:
            altered_file.setparams(new_params)

            # Read and write the audio frames
            frames = original_file.readframes(original_params.nframes)
            altered_file.writeframes(frames)

    print('File altered successfully.')

# Example usage:
alter_wav('Mercedes AMG C63s Drive Moderate V2.wav', new_bitdepth=32,
           new_channels=1,
             new_sample_rate=16000
             )
