const WebSocket = require('ws');
const fs = require('fs');

let elevator_music_buffer = fs.readFileSync('/root/back-end/walkie-talkie/altered_file.wav');
//let elevator_music_buffer = fs.readFileSync('/home/don/Documents/walkie/walkie-talkie/altered_file.wav');



const WS_PORT1 = process.env.WS_PORT || 3000;
const WS_PORT2 = process.env.WS_PORT || 4000;

const wsServer1 = new WebSocket.Server(
	{
		port: WS_PORT1,
		perMessageDeflate: {
			concurrencyLimit: 10,
			threshold: 1024
		}
	},
	() => console.log(`[Server] WS server is listening at ${WS_PORT}`)
);


const wsServer2 = new WebSocket.Server(
	{
		port: WS_PORT2,
		perMessageDeflate: {
			concurrencyLimit: 10,
			threshold: 1024
		}
	},
	() => console.log(`[Server] WS server is listening at ${WS_PORT}`)
);


//when the client connects
wsServer1.on("connection", (ws) => {
	console.log("[Client] Connected!" + ws.id);

    ws.timestamp = getTimestamp();
    ws.send(ws.timestamp)
	console.log(ws.timestamp)
        // console.log(wsServer.clients
    // console.log(elevator_music_buffer)
    // const bufferData = Buffer.from([0x48, 0x65, 0x6c, 0x6c, 0x6f]); // Example buffer
    const binaryData = elevator_music_buffer

     // Split the file data into chunks

        const chunkSize = 1024;
        for (let offset = 0; offset < elevator_music_buffer.length; offset += chunkSize) {
            const chunk = elevator_music_buffer.slice(offset, offset + chunkSize);
            console.log(chunk)
            sendData(chunk)

            // Send each chunk to the client
            //ws.send(chunk);
        }

    
    

    // sendData(binaryData)

	ws.on("close", () => {
		//saveSessionToFile(); // Call saveSessionToFile() here
		console.log("[Client] Disconnected: " + ws.id);
	  });
})





//when the client connects
wsServer2.on("connection", (ws) => {
	console.log("[Client] Connected!" + ws.id);

    ws.timestamp = getTimestamp();
    ws.send(ws.timestamp)
	console.log(ws.timestamp)
        // console.log(wsServer.clients
    // console.log(elevator_music_buffer)
    // const bufferData = Buffer.from([0x48, 0x65, 0x6c, 0x6c, 0x6f]); // Example buffer
    const binaryData = elevator_music_buffer

     // Split the file data into chunks

        const chunkSize = 1024;
        for (let offset = 0; offset < elevator_music_buffer.length; offset += chunkSize) {
            const chunk = elevator_music_buffer.slice(offset, offset + chunkSize);
            console.log(chunk)
            sendData(chunk)

            // Send each chunk to the client
            //ws.send(chunk);
        }

    
    

    // sendData(binaryData)

	ws.on("close", () => {
		//saveSessionToFile(); // Call saveSessionToFile() here
		console.log("[Client] Disconnected: " + ws.id);
	  });
})















function getTimestamp() {
	var last8digit_Timestamp = Date.now().toString().slice(-8);
	return parseInt(last8digit_Timestamp);
}


function sendData(data){
    console.log("send data has been called")
	wsServer2.clients.forEach(function each(client) {
    client.send(data)
    })
}
// // Create WebSocket server on port 3000
// const wss1 = new WebSocket.Server({ port: 3000 });

// wss1.on('connection', ws => {
//   // Handle WebSocket connections on port 3000
//   ws.on('message', message => {
//     console.log('Received on port 3000:', message);
//     ws.send('Response from port 3000');
//   });
// });

// // Create WebSocket server on port 4000
// const wss2 = new WebSocket.Server({ port: 4000 });

// wss2.on('connection', ws => {
//   // Handle WebSocket connections on port 4000
//   console.log("[Client] Connected!");
// //   client.send(elevator_music_buffer);
// //   ws.on('message', message => {
   
// //     //console.log('Received on port 4000:', message);
// //     ws.send('Response from port 4000');
// //   });
// });
