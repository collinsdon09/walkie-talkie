

# import asyncio
# import websockets
# import time

# dummy_buffer = 1

# async def hello():
#     async with websockets.connect("ws://localhost:8888") as websocket:
        
#         while True:
#             message = b"1"
#             await websocket.send(message)
#             time.sleep(1)



        
#         # # dummy_buffer = 1   
#         # while True:
#         #     message = await websocket.recv()
#         #     print(f"Received: {message}")
#         #     # Perform additional operations with the received message if needed

# asyncio.run(hello())
import asyncio
import websockets
import time

async def on_connect(websocket):
    print("Connected to the server.")

    # Send an initial message after connecting
    # while True:
    #     message = b"1"
    #     await websocket.send(message)
    #     time.sleep(1)
    message = b"1"
    await websocket.send(message)
    time.sleep(1)

async def on_message(websocket, message):
    print(f"Received: {message}")

    # Perform additional operations with the received message if needed

async def on_disconnect():
    print("Disconnected from the server.")

async def hello():
    async with websockets.connect("ws://localhost:4000") as websocket:
        await on_connect(websocket)

        while True:
            message = await websocket.recv()
            await on_message(websocket, message)

asyncio.run(hello())
