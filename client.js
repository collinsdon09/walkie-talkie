const WebSocket = require('ws');
const Speaker = require("speaker");
const Readable = require("stream").Readable;
const pcmSpeaker = new Readable();



pcmSpeaker.bitDepth = 32;
pcmSpeaker.channels = 1;
pcmSpeaker.sampleRate = 16000;
pcmSpeaker._read = bufRead;
pcmSpeaker.pipe(new Speaker());
let bytesRead = 0;
const bufferChunks = [];


function bufRead(buf) {
    

	if (buf instanceof Buffer) {
		pcmSpeaker.resume();
		pcmSpeaker.push(buf);

        bufferChunks.push(buf);
        //console.log(bufferChunks)
    //console.log(buf.byteLength)


		// console.log(buf)
	} else if (buf == null) {
        console.log("buffer is empty now!")
		pcmSpeaker.pause();
	}



    console.log("end")
}

// Create a new WebSocket instance
const socket = new WebSocket('ws://localhost:3000');

// Connection open event
socket.onopen = () => {
  console.log('WebSocket connection established.');
  
  // Send a message to the server
  socket.send('Hello, server!');
};

// Message received event
socket.onmessage = (event) => {
  const message = event.data;
  console.log('Received message:', message);
  bufRead(message)

 



};

// Connection close event
socket.onclose = (event) => {
  console.log('WebSocket connection closed with code:', event.code);
};

// Error event
socket.onerror = (error) => {
  console.error('WebSocket error:', error);
};
